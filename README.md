# synergy-client-fix-v2
Synergy combines your desktop devices together in to one cohesive experience. It's software for sharing your mouse and keyboard between multiple computers on your desk. It works on Windows, macOS and Linux.

The most recent version (v2) is riddled with issues and the community is not as excited as it once was about the product.

This script is a common fix for the Synergy client losing connection with host and connected machines.

# how to run script

cd /file_location <br>
sh synergy_reset.sh
