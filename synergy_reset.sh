launchctl unload /Library/LaunchAgents/com.symless.synergy.synergy-service.plist
sudo killall synergy-core
launchctl load /Library/LaunchAgents/com.symless.synergy.synergy-service.plist
open -n /Applications/Synergy.app

# Right-click on host machine and click "share from <machine name>"
# This may need to be done on any machine you are using synergy with
